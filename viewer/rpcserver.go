package main

import (
	"fmt"
	"net"
	"net/rpc"
	"strings"
)

type Worker int

type QuestionAnswer struct {
	Question string
	Answer   []string
}

type ArgRpc []QuestionAnswer

func (s *Worker) SetText(args ArgRpc, reply *int) error {
	*reply = 1
	//fmt.Println(args.Name)
	s1 := ""
	for _, v := range args {
		s1 = s1 + fmt.Sprintf("%v\n%v\n", v.Question, strings.Join(v.Answer, "\n"))
	}
	text.Set(s1)
	return nil
}

func textWaiter() error {
	worker1 := new(Worker)
	rpc.Register(worker1)
	l, e := net.Listen("tcp", "127.0.0.1:12336")
	if e != nil {
		return e
	}
	defer l.Close()

	rpc.Accept(l)

	return nil
}
