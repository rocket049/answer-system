package main

import (
	"os"
	"path/filepath"
	"runtime"
	"time"

	"fyne.io/fyne/v2"
	"fyne.io/fyne/v2/app"
	"fyne.io/fyne/v2/container"
	"fyne.io/fyne/v2/data/binding"
	"fyne.io/fyne/v2/widget"
)

func setFontPath() {
	exe1, err := os.Executable()
	if err != nil {
		panic(err)
	}
	path1 := filepath.Dir(exe1)
	fontpath := filepath.Join(path1, "font", "default.ttf")

	err = os.Setenv("FYNE_FONT", fontpath)
	if err != nil {
		panic(err)
	}
}

var text binding.String

func main() {
	setFontPath()
	defer os.Unsetenv("FYNE_FONT")
	text = binding.NewString()

	myApp := app.New()
	myWindow := myApp.NewWindow("AnswerViewer")

	board := widget.NewMultiLineEntry()
	board.Bind(text)
	board.Wrapping = fyne.TextWrapBreak
	board.PlaceHolder = "此处显示收到的文字。"

	content := container.NewPadded(board)

	myWindow.SetContent(content)
	//myWindow.SetFixedSize(true)
	myWindow.Resize(fyne.NewSize(480, 320))
	myWindow.CenterOnScreen()

	//rpc server
	go textWaiter()
	go func() {
		time.Sleep(time.Second * 3)

		if runtime.GOOS == "windows" {
			setWindowTopMost("AnswerViewer")
		}
	}()

	myWindow.ShowAndRun()
}
