package main

import (
	"net/rpc"
)

type Worker int

func showAnswers(answers []QuestionAnswer) error {
	var client *rpc.Client
	var err error

	client, err = rpc.Dial("tcp", "127.0.0.1:12336")
	if err != nil {
		return err
	}
	defer client.Close()
	var ret int
	err = client.Call("Worker.SetText", answers, &ret)
	return err
}
